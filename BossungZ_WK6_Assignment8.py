'''
Extract simple data from a webpage
using BeautifulSoup to extract website data
by Zack Bossung
'''

# Required Python Import Standard Library Modules
from __future__ import print_function

# Python Standard Libaries
import re
from io import BytesIO

# Python 3rd Party Libraries
import requests
import os
from bs4 import BeautifulSoup
from prettytable import PrettyTable
from PIL import Image

#Print out initial script header
print("\nBossungZ_WK6_Assignment8.py\nZack Bossung\nFebruary 2020\n")

#Create set and prettytable
urlSet = set()
emailSet = set()
imgURLSet = set()
outTable = PrettyTable()

#Define regEx to find email addresses
emailRegEx = re.compile('[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}')
#Define regEx to find URLs
urlRegEx = re.compile('((?:https:\/\/www\.|http:\/\/www\.|https:\/\/|http:\/\/|www\.)[a-zA-Z0-9-_]+\.[a-zA-Z]{2,4})')

url = str(input("Please input a website URL:\nExample: https://www.loandepot.com/loan-officers/jreininga\n"))
print()

#Scrape web page
scrapePage = requests.get(url)

#Create soupPage object by passing into beautifulsoup for parsing
soupPage = BeautifulSoup(scrapePage.text, 'html.parser')

#Get page title
title = soupPage.title.string

#Add page name as column header
outTable.field_names = [title]

###############################################
## Step through each item in page with a tag ##
## Test if each item matches URL regex       ##
##                                           ##
## Split each item on ':' delimeter and test ##
##    returned values against email regex    ##
##                                           ##
## Add matched items to respective set       ##
###############################################

for urlEmail in soupPage.findAll('a'):
    if re.match(urlRegEx, str(urlEmail.get('href'))) is not None:
        urlSet.add(urlEmail.get('href'))
        
    for email in str(urlEmail.get('href')).split(":"):
        if re.match(emailRegEx, email) is not None:
            emailSet.add(email)
            
###############################################
## Step through each set and add values to   ##
##    PrettyTable                            ##
###############################################
            
outTable.add_row(["----------- URL -----------"])
    
for outURL in urlSet:
    outTable.add_row([outURL])
    
outTable.add_row(["---------- Email ----------"])
    
for outEmail in emailSet:
    outTable.add_row([outEmail])
    
###############################################
## Verify that the image download folder     ##
##    exists, if not then create it          ##
###############################################
    
dirCreated = os.path.exists('WebPage_Out')

if dirCreated is False:
    os.mkdir('WebPage_Out')

###############################################
## Find all images within web page           ##
##                                           ##
## Get URL, verify that it is not None       ##
##                                           ##
## See if URL is relative and if so then     ##
##    prepend page URL                       ##
##                                           ##
## Finally add img URLs to table             ##
###############################################

images = soupPage.findAll('img')
for eachImage in images:
    imgURL = eachImage.get('src')
    if imgURL is not None:
        if imgURL[0:4] != 'http':
            imgURL = url+imgURL
        imgURLSet.add(imgURL)
        
outTable.add_row(["---------- Images ---------"])

for outImageURL in imgURLSet:
    outTable.add_row([outImageURL])
    try: 
        imgResponse = requests.get(outImageURL)                 # Get the image from the URL
        imageName = os.path.basename(outImageURL)
        img = Image.open(BytesIO(imgResponse.content))     # Download the image
        img.save('WebPage_Out' + "/" + imageName)                             # Save the image
    except Exception as err:
        print(outImageURL, ":", err)
        continue  

#Align column values to left side of cell
outTable.align[title] = "l"

outFile = open('WebPage_Out' + "/" + 'outFile.txt', 'w')
outFile.write(str(outTable))
outFile.close()

print(outTable)

input("Press [Enter] to exit")